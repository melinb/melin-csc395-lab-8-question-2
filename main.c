// main.c
//
// Author: Bjorn Melin
// Date: 3/31/2020


#ifdef VIRTUAL_SERIAL
#include <VirtualSerial.h>
#else
#warning VirtualSerial not defined, USB IO may not work
#define SetupHardware();
#define USB_Mainloop_Handler();
#endif

#include "common.h"
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "motor.h"
#include "leds.h"
#include "timers.h"

// starting time of ms_ticks to measure when the interrupt_counter started
volatile uint32_t start_ms_ticks;

// global var which keeps track of the reference position of 
// the motor, used to modify the position of the motor
volatile int16_t ref_pos = 0;   // note: signed so it can go negative
// used to keep track of motor moving forward or backwards
volatile uint8_t forward = 0;


/****************************************************************************
   ALL INITIALIZATION
****************************************************************************/
/**
 * Initializes the system
 */
void initialize_system(void) {
  // initalize green and yellow only.
	// initialization defines the IO_structs and sets DDR
	initialize_led(GREEN);
	initialize_led(YELLOW);
	// The "sanity check".
	// When you see this pattern of lights you know the board has reset
	light_show();
  setupMotor2();          // setup the motor
  setupEncoder();         // setup the encoder
  setup_ms_timer();
  motorForward();
  SetupHardware();        // This setups the USB hardware and stdio
}


/**
 * Function which blinks the YELLOW led.
 */
void blink_yellow() {
  TOGGLE_BIT(*(&_yellow)->port, _yellow.pin);
  _delay_ms(300);
  TOGGLE_BIT(*(&_yellow)->port, _yellow.pin);
}


/****************************************************************************
   MAIN
****************************************************************************/

int main(void) {
  USBCON = 0;             // This prevents the need to reset after flashing
  
	initialize_system();    // call the initialization function
  sei();                  // set global interrupt enable - needed for USB

  //////// CYCLIC CONTROL LOOP ////////
	// Main while loop
  while(1) {
    USB_Mainloop_Handler(); //Handles USB communication
    // set start time to current value of timer
    start_ms_ticks = ms_ticks;
    // reset interrupt counter to 0, incremented by 1 every time
    // encoder interrupt fires
    interrupt_counter = 0;  

    /**
     * while the motor has been running less than a second, do nothing
     */
    while (ms_ticks - start_ms_ticks <= 1000) {
    }   

    // once the motor has ran for a second, print the number
    // of times the encoder interrupt has fired
    printf("Interrupts:\t%ld\r\n", interrupt_counter);
    
  } /* end while(1) loop */
} /* end main() */
