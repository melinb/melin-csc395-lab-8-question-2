// timer.c
//
// Author: Bjorn Melin
// Date: 3/5/2020

#include "timers.h"
#include <avr/interrupt.h>


volatile uint64_t ms_ticks = 0;


/**
 * Initializes Timer 0 used for handling the Red LED.
 */
void setup_ms_timer(void) {
	// set timer control register part B to use timer 0
	// timer 0 is an 8-bit timer
	// Sets WGM0 to use CTC mode (hence the 01) 
	TCCR0A |= (1<<WGM01);

	// set timer counter control register part B to use timer 0
	// clock select 0 or 1 bits (uses 64 bits)
	TCCR0B |= (1<<CS00) | (1<<CS01);

	// timer 0 channel A
	// (16,000,000 ticks / 1 sec) * (1 cnt / 64 ticks) * (1 int / 250 cnt) = 1000int/sec
	// interrupt fires once every 1ms and increments ms_ticks e/ time
	OCR0A = 250;

	// Enables the interrupt OCIE0A
	TIMSK0 |= (1<<OCIE0A);
}

ISR(TIMER0_COMPA_vect){
    ms_ticks = ms_ticks + 1;
}