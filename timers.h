// timer.h
//
// Author: Bjorn Melin
// Date: 3/5/2020

#ifndef TIMERS_H_
#define TIMERS_H_

#include "common.h"
#include <util/delay.h>
#include <inttypes.h>
// #include "tasks.h"

extern volatile uint64_t ms_ticks;
// extern volatile uint64_t now;
// extern volatile uint64_t finish;
// extern volatile uint64_t wcet;

void setup_ms_timer(void);

#endif